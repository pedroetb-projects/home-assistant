# home-assistant

Deployment of Home Assistant, an open source home automation tool

## Configuration

To avoid compatibility issues, no `configuration.yaml` is provided, so you need to deploy `home-assistant` service without proper config in order to generate default files.

Then, you can stop the service and go to `config-vol` (named `home-assistant-config-vol` by default), where you will find `configuration.yaml` ready to be modified.

When all custom configs are in place, you can start `home-assistant` service again.

### HTTP (allow proxy)

When you access home-assistant using a domain name through a proxy, you need to explicitly allow that and declare trusted proxies addresses.

Append these lines into `configuration.yaml`:

```yaml
http:
  use_x_forwarded_for: true
  trusted_proxies:
    - 172.18.0.0/16
```

Note that `172.18.0.0/16` is the default network addresses range for `docker_gwbridge` network. If you are using another range, modify this value.

### Recorder (database)

This service is meant to use PostgreSQL as database, instead of default local SQLite.

You need to setup database service first (check [home-assistant-database](https://gitlab.com/pedroetb-projects/home-assistant-database)) and manually modify `configuration.yaml`.

Append these lines into it (note `RECORDER_DB_URL` is already defined as environment variable):

```yaml
recorder:
  db_url: !env_var RECORDER_DB_URL
```

Then, you can remove unused local database too: `rm home-assistant_v2.db*`
